CREATE TABLE categories (
	id					SERIAL PRIMARY KEY,
	category_name		TEXT UNIQUE NOT NULL,
);

CREATE TABLE links (
	category_id			INTEGER REFERENCES categories,
	parent_id			INTEGER REFERENCES categories
	PRIMARY KEY(category_id, parent_id)
);

CREATE TYPE ingredients_item AS (
	ingredient 			TEXT,
	amount 				NUMERIC(5, 2),
	unit_of_measure 	st_units_of_measure
);

CREATE TABLE dishes (
	dish_name			TEXT PRIMARY KEY,
	pictures			TEXT[],
	cooking_time		TIMESTAMP,
	instructions		TEXT,
	appliances_used		TEXT[]
);

CREATE TABLE ingredients (
	ingredient_name		TEXT PRIMARY KEY,
	description			TEXT,
	pictures			TEXT[],
	aka					TEXT[]
);

CREATE TABLE nonst_units_of_measure (
	unit_name			TEXT PRIMARY KEY,
	description			TEXT
);

CREATE TABLE dish_categories (
	dish 				TEXT NOT NULL REFERENCES dishes,
	category 			TEXT NOT NULL REFERENCES categories,
	PRIMARY KEY(dish, category) 
);

CREATE TABLE dish_ingredients (
	dish 				TEXT NOT NULL REFERENCES dishes,
	ingredient 			TEXT NOT NULL REFERENCES ingredients,
	amount 				NUMERIC(5, 2) NOT NULL,
	unit_of_measure 	st_units_of_measure NOT NULL,
	PRIMARY KEY(dish, ingredient)
);

CREATE TYPE st_units_of_measure AS ENUM(
	'Грамм',
	'Килограмм',
	'Литр',
	'Миллилитр'
);

CREATE TABLE measurement_compliance (
	nonstandard_unit	TEXT NOT NULL REFERENCES nonst_units_of_measure,
	ingredient 			TEXT NOT NULL REFERENCES ingredients,
	number_in_st_unit	NUMERIC(5, 2) NOT NULL,
	standard_unit		st_units_of_measure NOT NULL,	
	PRIMARY KEY(nonstandard_unit, ingredient, standard_unit)
);

/*
INSERT INTO units_of_measure (unit_name, standard) VALUES
	('Грамм', TRUE),
	('Килограмм', TRUE),
	('Миллилитр', TRUE),
	('Литр', TRUE),
	('Стакан', FALSE),
	('Чайная ложка', FALSE),
	('Столовая ложка', FALSE),
	('Единицы', FALSE)
;
*/

INSERT INTO nonst_units_of_measure (unit_name) VALUES
	('Стакан', ''),
	('Чайная ложка', ''),
	('Столовая ложка', ''),
	('Единицы', '')
;

INSERT INTO categories (category_name, prnt_category) VALUES 
	('Закуски', E'{"Блюда"}'),
	('Бульоны и супы', E'{"Блюда"}'),
	('Мясо', E'{"Блюда"}'),
	('Птица и дичь', E'{"Блюда"}'),
	('Рыба', E'{"Блюда"}'),
	('Молочные и яичные блюда', E'{"Блюда"}'),
	('Овощи и грибы', E'{"Блюда"}'),
	('Мучные блюда', E'{"Блюда"}'),
	('Крупяные блюда', E'{"Блюда"}'),
	('Сладкие блюда', E'{"Блюда"}'),
	('Напитки', E'{"Блюда"}'),
	('Отварное мясо', E'{"Мясо"}'),
	('Тушеное мясо', E'{"Мясо"}'),
	('Жареное мясо', E'{"Мясо"}'),
	('Запеченое мясо', E'{"Мясо"}'),
	('Соусы к мясным блюдам', E'{"Мясо"}')
;

INSERT INTO ingredients (ingredient_name, description, pictures, aka) VALUES
	('Мука', '', E'{}', E'{}'),
	('Сахарный песок', '', E'{}', E'{"Cахар"}'),
	('Рис', '', E'{}', E'{}'),
	('Соль', '', E'{}', E'{}'),
	('Говядина', '', E'{}', E'{}'),
	('Картофель', '', E'{}', E'{}'),
	('Морковь', '', E'{}', E'{}'),
	('Лук', '', E'{}', E'{}')
;

INSERT INTO measurement_compliance (nonstandard_unit, ingredient, number_in_st_unit, standard_unit) VALUES
	('Стакан', 'Мука', 160.00, 'Грамм'),
	('Столовая ложка', 'Мука', 25.00, 'Грамм'),
	('Чайная ложка', 'Мука', 10.00, 'Грамм'),
	('Стакан', 'Рис', 230.00, 'Грамм'),
	('Столовая ложка', 'Рис', 25.00, 'Грамм'),
	('Чайная ложка', 'Рис', 8.00, 'Грамм'),
	('Стакан', 'Сахарный песок', 100.00, 'Грамм'),
	('Столовая ложка', 'Сахарный песок', 25.00, 'Грамм'),
	('Чайная ложка', 'Сахарный песок', 10.00, 'Грамм'),
	('Стакан', 'Соль', 325.00, 'Грамм'),
	('Столовая ложка', 'Соль', 30.00, 'Грамм'),
	('Чайная ложка', 'Соль', 10.00, 'Грамм')
;

CREATE OR REPLACE FUNCTION make_a_recipe(dish TEXT, ctg TEXT[], ingr ingredients_item[],
											pict TEXT[], tm TIME, instr TEXT,
											appl TEXT[]) RETURNS void AS $$
DECLARE
	i TEXT;
BEGIN
	INSERT INTO dishes (dish_name, pictures, cooking_time, instructions, appliances_used) VALUES
		(dish, pict, tm, instr, appl);
	FOREACH i IN ARRAY ctg LOOP
		INSERT INTO dish_categories (dish, category) VALUES (dish, i);
	END LOOP;
	FOREACH i IN ARRAY ingr LOOP
		INSERT INTO dish_ingredients (dish, ingredient, amount, unit_of_measure) VALUES (dish, (i::ingredients_item).ingredient, (i::ingredients_item).amount, (i::ingredients_item).unit_of_measure);
	END LOOP;
END;
$$ language plpgsql;

SELECT make_a_recipe('Говядина отварная'::TEXT,
					E'{"Отварное мясо"}'::TEXT[],
					ARRAY[('Говядина'::TEXT, 500.00, 'Грамм'::TEXT)::ingredients_item, \
						('Картофель'::TEXT, 800.00, 'Грамм'::TEXT)::ingredients_item, \
						('Морковь'::TEXT, 150.00, 'Грамм'::TEXT)::ingredients_item, \
						('Лук'::TEXT, 150.00, 'Грамм'::TEXT)::ingredients_item]::ingredients_item[],
					E'{}'::TEXT[],
					'3:00'::TIME,
					'Промыть, довести до кипения, снять пену, варить на слабом огне 2.5 часа, за 30 минут \
					до готовности добавить нарезанные овощи'::TEXT, E'{"Кастрюля", "Нож", "Разделочная доска"}'::TEXT[]);